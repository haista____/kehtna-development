public class FibonacciOriginal {
	
	public static void main(String[] args) {
		long limit = Integer.parseInt(args[0]);
		long firstNumber = 0;
		long secondNumber = 1;
		System.out.print(firstNumber + " " + secondNumber + " ");
		for (long i = 0; i < (limit - 2); i++) {
			long newNumber = firstNumber + secondNumber;
			System.out.print(newNumber + " ");
			firstNumber = secondNumber;
			secondNumber = newNumber;
		}

	}
	
}