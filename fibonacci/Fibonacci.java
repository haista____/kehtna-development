public class Fibonacci {
	
	static int counter = 0;

	public static void main(String[] args) {
		long limit = Integer.parseInt(args[0]);
		long firstNumber = 0;
		long secondNumber = 1;
		System.out.print(firstNumber + " " + secondNumber + " ");
		printFibonacci(limit, firstNumber, secondNumber);
 
	}

	public static void printFibonacci(long limit, long firstNumber, long secondNumber) {
			if (counter < (limit -2)) {
				counter++;
				long newNumber = firstNumber + secondNumber;
				System.out.print(newNumber + " ");
				printFibonacci(limit, secondNumber, newNumber);
			}
	}
}