import java.util.Random;

public class MatrixApp {
	
	public static void main(String[] args) {
		MatrixApp app = new MatrixApp();
		int size = app.getRandomBetween(1, 10);
		int[][] matrix = app.getMatrix(size);
		app.printMatrix(matrix);
		System.out.println("Sorteeritud: ");
		app.printMatrix(app.sortMatrix(matrix, size));
	}

	public MatrixApp() {

	}
	
	private int[][] sortMatrix(int[][] matrix, int size) {
		for (int i = 0; i < size - 1; i++) {
			for (int j = i + 1; j < size; j++) {
					if (matrix[i][0] < matrix[j][0]) {
					int firstValue = matrix[i][0];
					matrix[i][0] = matrix[j][0];
					matrix[j][0] = firstValue;
			}
		}
	}

		for (int i = 0; i < size - 1; i++) {
			for (int j = i + 1; j < size; j++) {
					if (matrix[i][1] < matrix[j][1]) {
					int firstValue = matrix[i][1];
					matrix[i][1] = matrix[j][1];
					matrix[j][1] = firstValue;
			}
		}
	}

	return matrix;
}
	private int getRandomBetween(int min, int max) {
		Random random = new Random();
		int randomNumber = random.nextInt((max-min) + 1) + min;
		return randomNumber;
	}

	private int[][] getMatrix(int size) {
		int[][] matrix = new int[size][2];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < 2; j++) {
                matrix[i][j] = getRandomBetween(1, 10);
            }
		}
		return matrix;
	}

	private void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
		    for (int j = 0; j < matrix[i].length; j++) {
		        System.out.print(matrix[i][j] + " ");
		    }
	    	System.out.println();
		}
	}
}