import java.util.Random;

public class MatrixApp {
	
	public static void main(String[] args) {
		MatrixApp app = new MatrixApp();
		int size = app.getRandomBetween(3, 10);
		int minSizeOfNrs = Integer.parseInt(args[0]);
		int maxSizeOfNrs = Integer.parseInt(args[1]);
		int[][] matrix = app.getMatrix(size, minSizeOfNrs, maxSizeOfNrs);
		System.out.println("Originaalne: ");
		app.printMatrix(matrix);
		System.out.println("Sorteeritud: ");
		app.printMatrix(app.badlySortMatrix(matrix, size));
	}

	public MatrixApp() {

	}
	
	private int[][] sortMatrix(int[][] matrix, int size) {
		matrix = badlySortMatrix(matrix, size);
		/*
		int lastRow = size - 1;
		if (matrix[lastRow][1] < matrix[lastRow][0]) {
			int firstValue = matrix[lastRow][0];
			matrix[lastRow][0] = matrix[lastRow][1];
			matrix[lastRow][1] = firstValue;
			matrix = badlySortMatrix(matrix, size);
		}
		*/
		return matrix;
	}


	private int[][] badlySortMatrix(int[][] matrix, int size) {


		int[] array = new int[matrix.length * matrix[0].length];
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
					numbers[(i * matrix.length) + j] = matrix[i][j];
			}
		}

				// sort numbers

		for (int i = 0; i < array.length - 1; i++) {
			for (int j = i + 1; j < array.length; j++) {
				if (array[i] < array[j]) {
					int firstValue = array[i];
					array[i] = array[j];
					array[j] = firstValue;
				}			
			}
		}

				// placeback numbers into matrix

		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
					 matrix[i][j] = numbers[(i * matrix.length) + j]
			}
		}

			
		

		return matrix;
	}
	

	private int getRandomBetween(int min, int max) {
		Random random = new Random();
		int randomNumber = random.nextInt((max-min) + 1) + min;
		return randomNumber;
	}

	private int[][] getMatrix(int size, int minSizeOfNrs, int maxSizeOfNrs) {
		int[][] matrix = new int[size][2];
		for (int i = 0; i < size; i++) {
			for (int j = 0; j < 2; j++) {
                matrix[i][j] = getRandomBetween(minSizeOfNrs, maxSizeOfNrs);
            }
		}
		return matrix;
	}

	private void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
		    for (int j = 0; j < matrix[i].length; j++) {
		        System.out.print(matrix[i][j] + " ");
		    }
	    	System.out.println();
		}
	}
}