package com.example.emptyproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button buttonTest;
    private TextView historyView;
    String firstNumber = "x";
    String secondNumber = "x";
    String operator = "x";
    boolean alreadyCalculating = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        historyView = findViewById(R.id.TextView01);
    }

    public void onCalculatorButton(View view) {
        Button button = (Button) view;



        if (operator.equals("x")) {

            if (button.getText().equals(".")) {
                if (firstNumber.equals("x")) {
                    firstNumber = "0";
                }
            }

            if (firstNumber.equals("x")) {
                firstNumber = button.getText().toString();
            } else {
                firstNumber = firstNumber + button.getText().toString();
            }
            Toast.makeText(getApplicationContext(), firstNumber, Toast.LENGTH_LONG).show();
        } else {

            if (button.getText().equals(".")) {
                if (secondNumber.equals("x")) {
                    secondNumber = "0";
                }
            }

            if (secondNumber.equals("x") || alreadyCalculating) {
                secondNumber = button.getText().toString();
                alreadyCalculating = false;
            } else {
                secondNumber = secondNumber + button.getText().toString();
            }
            Toast.makeText(getApplicationContext(), secondNumber, Toast.LENGTH_LONG).show();
        }





    }

    public void onOperatorButton(View view) {
        Button button = (Button) view;
        operator = button.getText().toString();
    }

    public void onEqualsButton(View view) {
        double result;
        switch (operator) {
            case "+":
                result = Double.parseDouble(firstNumber) + Double.parseDouble(secondNumber);
                break;
            case "*":
                result = Double.parseDouble(firstNumber) * Double.parseDouble(secondNumber);
                break;
            case "-":
                result = Double.parseDouble(firstNumber) - Double.parseDouble(secondNumber);
                break;
            case "/":
                result = Double.parseDouble(firstNumber) / Double.parseDouble(secondNumber);
                break;
            default:
                result = 0;
        }
        Toast.makeText(getApplicationContext(), String.valueOf(result), Toast.LENGTH_LONG).show();
        historyView.append(String.valueOf(result) + "\n");
        firstNumber = String.valueOf(result);
        alreadyCalculating = true;
    }

    public void initialiseValues(View view) {
        operator = "x";
        firstNumber = "x";
        secondNumber = "x";
        Toast.makeText(getApplicationContext(), "Cleared!", Toast.LENGTH_LONG).show();
    }

    public void deleteHistory(View view) {
        historyView.setText("");
    }

    public void newIntent(View view) {



        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("Result", "5");
        startActivity(intent);

    }


}

