package com.example.todoapp;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {


    private List<TODOTask> tasks;
    private ListView listView;
    private ArrayAdapter<TODOTask> listAdapter;
    private Button buttonAdd;
    private EditText inputTask;
    private FirebaseAuth auth;
    private DatabaseReference database;

    private RESTService restService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        String email = "test@test.ee";
        String password = "123456";
        registerUser(email, password);
        logIn(email, password);
        database = FirebaseDatabase.getInstance().getReference();

        buttonAdd = findViewById(R.id.button_add);
        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String description = inputTask.getText().toString();
                int id = listAdapter.getCount() + 1;
                if (!description.trim().isEmpty()) {
                    TODOTask newTask = new TODOTask(description, id);
                    listAdapter.add(newTask);
                    inputTask.setText("");
                    listView.setSelection(listAdapter.getCount() - 1);
                    addTask(newTask);
                    listAdapter.clear();
                }
            }
        });


        inputTask = findViewById(R.id.input_task);

        listView = findViewById(R.id.list_tasks);


        tasks = new ArrayList<>();
       // tasks.add(new TODOTask("Call mother"));
       // tasks.add(new TODOTask("Finish omework"));
       // tasks.add(new TODOTask("send cv"));
       // tasks.add(new TODOTask("99 agility"));



        //listAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, tasks);
        listAdapter = new CustomArrayAdapter(this, tasks);
        listView.setAdapter(listAdapter);

/*
        Retrofit retrofit = new Retrofit.Builder().baseUrl("https://todo-app-574a9.firebaseio.com/").addConverterFactory(GsonConverterFactory.create()).build();

        restService = retrofit.create(RESTService.class);
        getTasks();
        */
    }

    public void getTasks() {
        Call<List<TODOTask>> call = restService.getTasks();
        call.enqueue(new Callback<List<TODOTask>>() {
            @Override
            public void onResponse(Call<List<TODOTask>> call, Response<List<TODOTask>> response) {
                List<TODOTask> tasks = response.body();
                if (tasks != null && tasks.size() > 0) {
                    for (TODOTask task : tasks) {
                        listAdapter.add(task);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<TODOTask>> call, Throwable t) {
                Log.d("todoapp", t.getMessage());
            }
        });
    }

    public void updateTask(TODOTask task) {
        Call<TODOTask> call = restService.updateTask(task.id, task);
        call.enqueue(new Callback<TODOTask>() {
            @Override
            public void onResponse(Call<TODOTask> call, Response<TODOTask> response) {
                Toast.makeText(getApplicationContext(), "Checkbox changed!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<TODOTask> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Unknown error.", Toast.LENGTH_SHORT).show();
            }
        });
    }
/*
    public void addTask(TODOTask task) {
        Call<TODOTask> call = restService.addTask(task);
        call.enqueue(new Callback<TODOTask>() {
            @Override
            public void onResponse(Call<TODOTask> call, Response<TODOTask> response) {
                Toast.makeText(getApplicationContext(), "Succesfully added!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<TODOTask> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Unknown error.", Toast.LENGTH_SHORT).show();
            }
        });
    }
*/
    public void deleteTask(TODOTask task) {
        Call<TODOTask> call = restService.deleteTask(task.id);
        call.enqueue(new Callback<TODOTask>() {
            @Override
            public void onResponse(Call<TODOTask> call, Response<TODOTask> response) {
                Toast.makeText(getApplicationContext(), "Succesfully deleted!", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<TODOTask> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Unknown error.", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void registerUser(String username, String password) {
        auth = FirebaseAuth.getInstance();
        auth.createUserWithEmailAndPassword(username, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull com.google.android.gms.tasks.Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "its oke.", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getApplicationContext(), "no ok", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void logIn(String username, String password) {
        auth = FirebaseAuth.getInstance();
        auth.signInWithEmailAndPassword(username, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(getApplicationContext(), "login ok", Toast.LENGTH_SHORT).show();
                    loadAllTasks();
                } else {
                    Toast.makeText(getApplicationContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void loadAllTasks() {
        database.child("tasks").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    TODOTask task = snapshot.getValue(TODOTask.class);
                    listAdapter.add(task);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void addTask(TODOTask task) {
        task.key = database.child("tasks").push().getKey();
        database.child("tasks").child(task.key).setValue(task);
    }

}
