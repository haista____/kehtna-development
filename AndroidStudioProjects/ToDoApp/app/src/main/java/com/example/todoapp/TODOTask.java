package com.example.todoapp;

import java.util.Calendar;
import java.util.Date;

public class TODOTask {


    public boolean isCompleted;
    public int id;
    public String key;
    public long createdDate;
    public String description;

    public TODOTask() {}

    public TODOTask(String description, int id) {
        this.description = description;
        isCompleted = false;
        createdDate = Calendar.getInstance().getTime().getTime();
        this.id = id;
    }


}
