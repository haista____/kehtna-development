package com.example.todoapp;

import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class CustomArrayAdapter extends ArrayAdapter<TODOTask> {

    private MainActivity mainActivity;

    public CustomArrayAdapter(MainActivity mainActivity, List<TODOTask> tasks) {
        super(mainActivity.getApplicationContext(), 0, tasks);
        this.mainActivity = mainActivity;
    }


    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        TaskViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_task, parent, false);
            viewHolder = new TaskViewHolder();
            viewHolder.id = convertView.findViewById(R.id.text_id);
            viewHolder.date = convertView.findViewById(R.id.text_date);
            viewHolder.description = convertView.findViewById(R.id.text_desc);
            viewHolder.delete = convertView.findViewById(R.id.button_delete);
            viewHolder.chkbox = convertView.findViewById(R.id.chkbox_isCompleted);
            viewHolder.add = convertView.findViewById(R.id.button_add);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (TaskViewHolder) convertView.getTag();
        }

        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), String.valueOf(position), Toast.LENGTH_SHORT).show();
                TODOTask task = getItem(position);
                remove(task);
                mainActivity.deleteTask(task);
            }
        });

        viewHolder.chkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                TODOTask task = getItem(position);
                task.isCompleted = isChecked;
                mainActivity.updateTask(task);
            }
        });



        TODOTask task = getItem(position);
        viewHolder.id.setText(String.valueOf(task.key));
        viewHolder.description.setText(task.description);
        viewHolder.date.setText(DateFormat.format("dd-MM-yyyy (HH:mm:ss)", task.createdDate));

        return convertView;
    }

    public class TaskViewHolder {
        public TextView description;
        public TextView date;
        public TextView id;
        public Button delete;
        public CheckBox chkbox;
        public Button add;
    }
}
