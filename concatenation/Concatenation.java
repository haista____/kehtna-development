import java.lang.*;

public class Concatenation {
	//char counter
	public static void main(String[] args) {
		String myString = args[0];
		String outputString = "";
		int count = 1;
		char[] myCharArray = myString.toCharArray();
		for (int i = 0; i < myCharArray.length; i++) {
			// kui oleme lõpus, kontrollime kas midagi loeme
			//ei loe midagi, läheb praegune olemasolev string + viimane char
			if (i == (myCharArray.length - 1) && (count == 1)) { 
				outputString = outputString + myCharArray[i];
			//loeme, läheb praegune olemasolev string + viimase chari arv
			} else if (i == (myCharArray.length - 1) && (count > 1)) { 
				outputString = outputString + count + myCharArray[i];
			//ei ole lõpus
			} else {
				//praeguse indexiga char võrdub järgmise chariga
				if (Character.toLowerCase(myCharArray[i]) == Character.toLowerCase(myCharArray[i+1])) {
					count++;
				//praegune ja järgmine char on erinevad
				} else {
					//kui loeme juba midagi, lisame outputile olemasoleva loendi arvu pluss olemasolev string
					if (count > 1) {
						outputString = outputString + count + myCharArray[i];
						count = 1; //resettime loenduse
					//ei loe midagi, läheb lihtsalt olemasolev string pluss järgmine string
					} else { 
						outputString = outputString + myCharArray[i];
					}
				}
			}
		}
		System.out.println(outputString);
	}	
}