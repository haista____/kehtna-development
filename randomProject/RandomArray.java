import java.util.Random;

public class RandomArray {
	
	public static void main(String[] args) {
		int sizeOfArray = Integer.parseInt(args[0]);
		int minNumber = Integer.parseInt(args[1]);
		int maxNumber = Integer.parseInt(args[2]);
		int ascending = Integer.parseInt(args[3]);
		int[] randomArray = new int[sizeOfArray];
		for (int i = 0; i < sizeOfArray; i++) {
			randomArray[i] = getRandomBetween(minNumber, maxNumber);
		}

		//print the original array

		for (int i = 0; i < randomArray.length; i++) {
			System.out.print(randomArray[i] + " ");
		}
		//print the sorted array
		System.out.println();
		int[] sortedArray = sortThatShit(randomArray, ascending);
		for (int i = 0; i < sortedArray.length; i++) {
			System.out.print(sortedArray[i] + " ");
		}
	}

	public static int getRandomBetween(int min, int max) {
		Random random = new Random();
		int randomNumber = random.nextInt((max-min) + 1) + min;
		return randomNumber;
	}
	

	public static int[] sortThatShit(int[] array, int asc) {
		for (int i = 0; i < array.length - 1; i++) {
			for (int j = i + 1; j < array.length; j++) {
				if (asc == 1) {
					if (array[i] < array[j]) {
					int firstValue = array[i];
					array[i] = array[j];
					array[j] = firstValue;
					}
				} else {
					if (array[i] > array[j]) {
					int firstValue = array[i];
					array[i] = array[j];
					array[j] = firstValue;
					}
				}
			}
		}
		return array;
	}
}