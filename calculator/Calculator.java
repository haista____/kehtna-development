import java.util.Scanner;

public class Calculator {

	public static void main(String[] args) {
		int firstNumber = 0;
		int secondNumber = 0;
		String parameter = "";
		if (args.length == 0) {
			try {
			System.out.println("You forgot your parameters, boy");
			Scanner scanner = new Scanner(System.in);
			System.out.print("Enter the first number you want to calculate with: ");
			firstNumber = Integer.parseInt(scanner.next());
			System.out.print("Enter the operator for the operation you want to calculate: ");
			parameter = scanner.next();
			System.out.print("Enter the second number: ");
			secondNumber = Integer.parseInt(scanner.next());
		} catch (Exception e) {
			System.out.println("You entered something stupid");
		}

		} else {
			try {
			firstNumber = Integer.parseInt(args[0]);
			secondNumber = Integer.parseInt(args[2]);
			parameter = args[1];
			} catch (Exception e) {
				System.out.println("You entered something stupid");
			}
		}

		
		switch(parameter) {
			case "X":
			case "x":
			case "*":
					System.out.println(multiply(firstNumber, secondNumber));
					break;
			case "+":
					System.out.println(add(firstNumber, secondNumber));
					break;
			case "-":
					System.out.println(subtract(firstNumber, secondNumber));
					break;
			case "/":
					System.out.println(divide(firstNumber, secondNumber));
					break;
			default:
					System.out.println("The operator is not valid");
					break;
			}
	}

	public static int multiply(int first, int second) {
		return (first * second);
	}

	public static int add(int first, int second) {
		return (first + second);
	}

	public static int subtract(int first, int second) {
		return (first - second);
	}

	public static float divide(int first, int second) {
		return ((float) first / second);
	}
	
}