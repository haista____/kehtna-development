import java.util.Random;
import java.util.Scanner;
import java.io.IOException;
import java.lang.*;

public class NavalBattle implements Runnable {
	
	int score = 0;
	static boolean isRunning = true;
	static int rows = 0;
	static int columns = 0;


	public static void main(String[] args) throws IOException, InterruptedException {
		NavalBattle app = new NavalBattle(args[0], args[1], args[2]);
		Thread thread = new Thread(app);
		thread.start();
	}

	public NavalBattle(String args1, String args2, String args3) throws IOException, InterruptedException {
		rows = Integer.parseInt(args1);
		columns = Integer.parseInt(args2);
		int numberOfShips = Integer.parseInt(args3);
		getUserInput(getBoardMatrix(rows, columns, numberOfShips));
	}

	public void run() {
		int counter = 1;
		while (isRunning) {
		try {
			System.out.println("Time: " + counter);
			Thread.sleep(1000);
			counter++;
		} catch (InterruptedException ie) {
			ie.printStackTrace();
		}

	}
	
	}

	private int[][] getBoardMatrix(int rows, int columns, int numberOfShips) {
		int[][] matrix = new int[rows][columns];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
                matrix[i][j] = getRandomBetween(1, numberOfShips);
            }
		}
		return matrix;
	}

	private int getNumberOfShips(int[][] matrix) {
		int number = 0;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
                if (matrix[i][j] == 1) {
                	number++;
                } 
            }
		}
		return number;
	}

	private String[][] getUserBoardMatrix(int rows, int columns) {
		String[][] matrix = new String[rows][columns];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
                matrix[i][j] = "X";
            }
		}
		return matrix;
	}

	private void getUserInput(int[][] matrix) throws IOException, InterruptedException {
		int originalNumberOfShips = getNumberOfShips(matrix);
		String[][] userMatrix = getUserBoardMatrix(rows, columns);
		long startTime = System.currentTimeMillis();
		System.out.println("Number of ships: " + originalNumberOfShips);
		
		while (score < originalNumberOfShips) {
			
			printBoard(userMatrix);
			Scanner scanner = new Scanner(System.in);
			System.out.print("Enter the column: ");
			int columnNr = Integer.parseInt(scanner.next());
			System.out.print("Enter the row: ");
			int rowNr = Integer.parseInt(scanner.next());

			if (columnNr == 42069) {
				score = originalNumberOfShips;
			} 
			if (rowNr > rows) {
				rowNr = rows;
			}
			if (columnNr > columns) {
				columnNr = columns;
			}
			if (columnNr == 0) {
				columnNr = 1;
			}
			if (rowNr == 0) {
				rowNr = 1;
			}
			if (columnNr < 0) {
				columnNr = 1;
			}
			if (rowNr < 0) {
				rowNr = 1;
			}


			if (matrix[rowNr - 1][columnNr - 1] == 1) {
				userMatrix[rowNr - 1][columnNr - 1] = "1";
				matrix[rowNr - 1][columnNr - 1] = -1;
				countScore(true, false, matrix, originalNumberOfShips);
			} else if (matrix[rowNr - 1][columnNr - 1] == -1) {
				countScore(false, true, matrix, originalNumberOfShips);
			} else {
				userMatrix[rowNr - 1][columnNr - 1] = "0";
				countScore(false, false, matrix, originalNumberOfShips);
			}
		}
		isRunning = false;
		long endTime = System.currentTimeMillis();
		double secondsTaken = (endTime - startTime) / 1000;
		System.out.println("Congratulaions! U are winner.");
		System.out.println("Time taken: " + secondsTaken + " seconds.");
	}

	private void countScore(boolean isHit, boolean isAlreadyHit, int[][] matrix, int realShipNumber) throws IOException, InterruptedException {
		new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
		if (isHit && !isAlreadyHit) {
			score++;
			System.out.println("Hit! - Score: " + score + "/" + realShipNumber);
		} else if (isAlreadyHit) {
			System.out.println("Already hit!");
		} else {
			System.out.println("Miss! - Score: " + score + "/" + realShipNumber);
		}

	}

	private void printBoard(String[][] matrix) {
		for (int i = 0; i < matrix[0].length; i++) {
			System.out.print((i + 1) + " ");
		}
		System.out.println("");
		for (int i = 0; i < matrix.length; i++) {
		    for (int j = 0; j < matrix[i].length; j++) {
		        System.out.print(matrix[i][j] + "|");
		    }
	    	System.out.println((i + 1)+ "");
		}
	}

	private int getRandomBetween(int min, int max) {
		Random random = new Random();
		int randomNumber = random.nextInt((max-min) + 1) + min;
		return randomNumber;
	}



}