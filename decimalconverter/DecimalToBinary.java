public class DecimalToBinary {
	
	public static void main(String[] args) {
		int decimal = 0;
		try {
			decimal = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			System.out.println("That's not a number");
			System.exit(1);
		}
		String binary = "";
		while (decimal > 1) {
				if ((decimal % 2) == 1) {
					binary = "1" + binary;
				} else {
					binary = "0" + binary;
				}
				decimal = decimal / 2;
		}
		binary = "1" + binary;
		if (decimal == 0) {
			System.out.println("0");
		} else {
			System.out.println(binary);
		}
	}
	
}